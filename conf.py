#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
import os
import shlex
import sphinx_theme_pd

extensions = [
    'sphinx.ext.mathjax',
]

templates_path = ['_templates']

source_suffix = ['.md', '.rst']

master_doc = 'index'

project = 'git-manual'
copyright = '2016, Yusuke Sasaki'
author = 'Yusuke Sasaki'
version = '0.0.1'
release = '0.0.1'

language = 'ja'

exclude_patterns = ['_build']

pygments_style = 'trac'

todo_include_todos = False


html_theme = 'sphinx_theme_pd'
html_theme_path = [sphinx_theme_pd.get_html_theme_path()]

html_static_path = ['_static']

htmlhelp_basename = 'git-manualdoc'

# -- Options for LaTeX output ---------------------------------------------

latex_elements = {
# The paper size ('letterpaper' or 'a4paper').
#'papersize': 'letterpaper',

# The font size ('10pt', '11pt' or '12pt').
#'pointsize': '10pt',

# Additional stuff for the LaTeX preamble.
#'preamble': '',

# Latex figure (float) alignment
#'figure_align': 'htbp',
}

# Grouping the document tree into LaTeX files. List of tuples
# (source start file, target name, title,
#  author, documentclass [howto, manual, or own class]).
latex_documents = [
  (master_doc, 'git-manual.tex', 'git-manual Documentation',
   'Yusuke Sasaki', 'manual'),
]

# The name of an image file (relative to this directory) to place at the top of
# the title page.
#latex_logo = None

# For "manual" documents, if this is true, then toplevel headings are parts,
# not chapters.
#latex_use_parts = False

# If true, show page references after internal links.
#latex_show_pagerefs = False

# If true, show URL addresses after external links.
#latex_show_urls = False

# Documents to append as an appendix to all manuals.
#latex_appendices = []

# If false, no module index is generated.
#latex_domain_indices = True
