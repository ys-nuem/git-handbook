Gitによるプロジェクト管理入門
=============================

具体的な例を交えてGitの作業手順を説明します．
本資料ではGitのインストール自体は説明しないため各自インストールしておいてください．
また，本資料ではコマンドラインでの作業を全体に説明を行います．
別のツールを使用する場合は適宜読み替えてください．

まずは作業用のリポジトリを作成します．
次の手順で新しくリポジトリを作成しておいてください．

::

    $ mkdir example1
    $ cd example1
    $ git init
    Initialized empty Git repository in /path/to/example1/.git/

コミットの作成者を設定しておく．

::

    $ git config --global user.name "Yusuke Sasaki"
    $ git config --global user.email "yusuke.sasaki@example.com"

Gitでの作業フロー
-----------------

Gitでの基本的な作業手順は次のようになります．

1. ファイルを編集する
2. 変更内容をステージに追加する ``git add``
3. ステージの変更内容をコミットする ``git commit``

コミット
--------

コミットの手順は次のようになります．

::

    $ edit hogehoge.txt
    $ git add hogehoge.txt
    $ git commit -m "edit hogehoge.txt."

``-m "message"`` でコミットに注釈をつけることができる．
Subversionとは異なり，Gitは原則コミットに注釈をつけることが必須である．

--------------

コミット履歴を表示するには\ ``git log``\ を用いる．

::

    $ git log
    commit ccf5155f4976ee25c8373e0f958301b298d538a4
    Author: Yusuke Sasaki <y_sasaki@nuem.nagoya-u.ac.jp>
    Date:   Sun Apr 10 16:04:06 2016 +0900

        Initial commit

1行目に記載されているのは\ **コミットハッシュ**\ と呼ばれるものであり，
コミットごとに固有に割り当てられる．
SubversionでいうリビジョンIDと同様のものであるが，Gitはリポジトリを集中的に
管理していない関係上，複数人が同じリビジョンIDを割り当てることを防止するためにこのような
方法を用いている．

2行目にはコミット作成者，3行目はコミット作成日時が記載されている．
4行目以降はコミットメッセージが記載される．
