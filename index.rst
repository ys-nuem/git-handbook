.. git-manual documentation master file, created by
   sphinx-quickstart on Tue Apr 12 16:19:04 2016.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to git-manual's documentation!
======================================

Contents:

.. toctree::
   :maxdepth: 2

   intro
   git-local


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

